<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title>
<?php wp_title(); ?>
<?php bloginfo('name'); ?>
</title>
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link href="style.css" rel="stylesheet" type="text/css" />
<?php if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' ); 
		wp_head();?>
</head>
<body <?php body_class(); ?>>
<div id="header">
  <h1><a href="<?php echo home_url();?>">
    <?php bloginfo('name');?>
    </a></h1>
  <div id="description">
    <?php bloginfo('description');?>
  </div>
  <div id="searchTop">
    <form method="get" action="<?php echo home_url(); ?>/">
      <input type="submit" id="searchsubmit" class="btnSearch" value="GO" />
      <input type="text" value="<?php the_search_query(); ?>" name="s" id="s" class="txtField" />
    </form>
  </div>
</div>
<div id="nav">
  <ul>
    <li><a href="<?php echo home_url(); ?>/">
      <?php _e('home','warm-home');?>
      </a></li>
    <?php wp_list_pages('depth=1&title_li='); ?>
  </ul>
</div>
