<?php get_header();?>

<div id="content">
  <div class="main">
    <div class="top"></div>
    <div class="center">
      <?php if (have_posts()) : ?>
      <?php while (have_posts()) : the_post(); ?>
      <div class="posts" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?/*?><h1 class="singleTitle"><?*/?>
		<h1>
          <?php the_title(); ?>
        </h1>
        <div class="postmetadata">
          <?php _e('Filed under&#58;','warm-home');?>
          <?php the_tags('Tags: ', ', ', '<br />'); ?>
          Posted in
          <?php the_category(', ') ?>
          |
          <?php _e('by','warm-home');?>
          <?php the_author();?>
        </div>
        <div class="entry">
          <?php the_content();?>
          <?php edit_post_link( __( 'Edit', 'warm-home' ), '<span class="edit-link">', '</span>' ); ?>
          <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'warm-home' ), 'after' => '</div>' ) ); ?>
        </div>
      </div>
      <!--post end-->
      
      <div class="navigation">
        <div class="floatright">
          <?php previous_post_link('%link'); ?>
        </div>
        <div class="floatleft">
          <?php next_post_link('%link'); ?>
        </div>
      </div>
      <?php comments_template(); ?>
      <?php endwhile; ?>
      <?php else : ?>
      <div class="post" id="post-<?php the_ID(); ?>">
        <h3>
          <?php _e('Not Found','warm-home');?>
        </h3>
        <?php _e('<p>Sorry, but you are looking for something that isn\'t here.</p>', 'warm-home') ?>
        <?php _e('<p>Or you can click the links under these:</p>', 'warm-home') ?>
        <h2>
          <?php _e('Archives','warm-home');?>
        </h2>
        <?php wp_get_archives('type=monthly');?>
        <?php _e('<p>Or you can use the searchform:</p>', 'warm-home') ?>
        <h2>
          <?php _e('search','warm-home');?>
        </h2>
        <?php get_search_form() ;?>
      </div>
      <?php endif; ?>
    </div>
    <!--main.center end-->
    <div class="bot"></div>
  </div>
  <!--main end-->
  <?php get_sidebar(); ?>
</div>
<!--content end-->
<?php get_footer(); ?>
