<?php get_header();?>



<div id="content">

  <div class="main">

    <div class="top"></div>

    <div class="center">

      <div class="posts" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <h3><?php _e('Error 404 - Not Found','warm-home'); ?></h3>
        <?php _e('<p>Sorry, but you are looking for something that isn\'t here.</p>', 'warm-home') ?>
         <?php _e('<p>Or you can click the links under these:</p>', 'warm-home') ?>
         <h2>
          <?php _e('Archives','warm-home');?>
        </h2>
         <?php wp_get_archives('type=monthly');?>
 
 <?php _e('<p>Or you can use the searchform:</p>', 'warm-home') ?>       
<h2>
    <?php _e('search','warm-home');?>
    </h2>
    <div class="search">
      <?php get_search_form() ;?>
      </div>
      </div>

     

    </div>

    <!--main.center end-->

    <div class="bot"></div>

  </div>

  <!--main end-->
  <?php get_sidebar (); ?>
</div>

<!--content end-->

<?php get_footer(); ?>

