<?php get_header();?>

<div id="content">
  <div class="main">
    <div class="top"></div>
    <div class="center">
      <?php if (have_posts()) : ?>
      <?php while (have_posts()) : the_post(); ?>
      <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <ul class="posts-quick">
          <li>
            <div class="commentnum">
              <?php /*comments_popup_link('0', '1', '%');*/ ?>
            </div>
            <div class="entry-thumbnails"><a href="<?php the_permalink();?>">
              <?php warmHome_the_thumbnail();?>
              </a></div>
            <h3><a href="<?php the_permalink();?>" rel="bookmark" title="<?php the_title(); ?>">
              <?php the_title(); ?>
              </a></h3>
            <div class="entry-summary">
              <div class="postmetadata">
                <?php _e('Posted in&#58;','warm-home');?>
                <?php the_category(', ') ?>
                &nbsp;|&nbsp;
                <?php _e('By','warm-home');?>
                <?php the_author();?>
                &nbsp;|&nbsp;
                <?php the_time('F j, Y'); ?>
                at
                <?php the_time('g:i a'); ?>
              </div>
              <div class="entry">
                <?php the_excerpt(); ?> 
                <p class="quick-read-more"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                  <?php _e('Celý článek >>', 'warm-home');?>
                  </a></p>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <!--post end-->
      <?php endwhile; ?>
      <div class="navigation">
        <?php posts_nav_link(); ?>
      </div>
      <?php else : ?>
      <div class="post" id="post-<?php the_ID(); ?>">
        <h3>
          <?php _e('Not Found','warm-home');?>
        </h3>
        <?php _e('<p>Sorry, but you are looking for something that isn\'t here.</p>', 'warm-home') ?>
        <?php _e('<p>Or you can click the links under these:</p>', 'warm-home') ?>
        <h2>
          <?php _e('Archives','warm-home');?>
        </h2>
        <?php wp_get_archives('type=monthly');?>
        <?php _e('<p>Or you can use the searchform:</p>', 'warm-home') ?>
        <h2>
          <?php _e('search','warm-home');?>
        </h2>
        <?php get_search_form() ;?>
      </div>
      <?php endif; ?>
    </div>
    <!--main.center end-->
    <div class="bot"></div>
  </div>
  <!--main end-->
  <?php get_sidebar(); ?>
</div>
<!--content end-->
<?php get_footer(); ?>
